#! /usr/bin/python

import os
import shlex
import sys
import time
import json

import pyroute2
import ovs_util

CNI_VERSION = "0.3.1"

class JAGUARCNIException(Exception):

    def __init__(self, code, message, details=None):
        super(JAGUARCNIException, self).__init__("%s - %s" % (code, message))
        self._code = code
        self._msg = message
        self._details = details

    def cni_error(self):
        error_data = {'cniVersion': CNI_VERSION,
                      'code': self._code,
                      'message': self._msg}
        if self._details:
            error_data['details'] = self._details
        return json.dumps(error_data)


def setup_interface(container_id, cni_netns, cni_ifname,
                    mac_address, ip_address, gateway_ip):
    try:
        if not os.path.exists("/var/run/netns"):
            os.makedirs("/var/run/netns")
    except Exception as e:
        raise JAGUARCNIException(100, "failure in creation of netns directory")

    try:
        ipdb = pyroute2.IPDB(mode='explicit')
        veth_outside = container_id[:15]
        veth_inside = container_id[:13] + "_c"
        ipdb.create(ifname=veth_outside, kind='veth', peer=veth_inside)
        with ipdb.interfaces[veth_outside] as veth_outside_iface:
            # Up the outer interface
            veth_outside_iface.up()
            veth_outside_idx = veth_outside_iface.index

        # Create a link for the container namespace
        # This is necessary also when using pyroute2
        # See https://github.com/svinota/pyroute2/issues/290
        netns_dst = "/var/run/netns/%s" % container_id
        if not os.path.isfile(netns_dst):
            command = "ln -s %s %s" % (cni_netns, netns_dst)
            call_popen(shlex.split(command))

        with ipdb.interfaces[veth_inside] as veth_inside_iface:
            # Move the inner veth inside the container namespace
            veth_inside_iface.net_ns_fd = container_id

    except Exception as e:
        raise JAGUARCNIException(100, "veth pair setup failure")

    try:
        # Change the name of veth_inside to $cni_ifname
        ns_ipdb = pyroute2.IPDB(nl=pyroute2.NetNS(container_id),
                                mode='explicit')
        # Configure veth_inside: set name, mtu, mac address, ip, and bring up
        with ns_ipdb.interfaces[veth_inside] as veth_inside_iface:
            veth_inside_iface.ifname = cni_ifname
            veth_inside_iface.address = mac_address
            veth_inside_iface.mtu = config.get_option('mtu')
            veth_inside_iface.add_ip(ip_address)
            veth_inside_iface.up()

        # Set the gateway
        ns_ipdb.routes.add(dst='default', gateway=gateway_ip).commit()

        return veth_outside
    except Exception as e:
        if veth_outside_idx:
            pyroute2.IPRoute().link('del', index=veth_outside_idx)
        raise JAGUARCNIException(100, "container interface setup failure")


def cni_add(cni_ifname, cni_netns, namespace, pod_name, container_id):
    ip_address = '172.17.10.2'
    mac_address = '00:00:00:00:00:01'
    gateway_ip = '172.17.10.1'

    veth_outside = setup_interface(container_id, cni_netns, cni_ifname,
                                   mac_address, ip_address,
                                   gateway_ip)

    iface_id = "%s_%s" % (namespace, pod_name)

    try:
        ovs_vsctl('add-port', 'br0', veth_outside, '--', 'set',
                  'interface', veth_outside,
                  'external_ids:attached_mac=%s' % mac_address,
                  'external_ids:iface-id=%s' % iface_id,
                  'external_ids:ip_address=%s' % ip_address)
    except Exception:
        raise JAGUARCNIException(106, "failure in plugging pod interface")

    output = json.dumps({'ip_address': ip_address,
                        'gateway_ip': gateway_ip, 'mac_address': mac_address})
    print(output)

def cni_del(container_id):
    try:
        ovs_vsctl("del-port", container_id[:15])
    except Exception:
        message = "failed to delete OVS port %s" % container_id[:15]
        print(message)

    command = "rm -f /var/run/netns/%s" % container_id
    call_popen(shlex.split(command))

def main():
    try:
        cni_command = os.environ['CNI_COMMAND']
        cni_ifname = os.environ['CNI_IFNAME']
        cni_netns = os.environ['CNI_NETNS']
        cni_args = os.environ['CNI_ARGS']

        cni_args_dict = dict(i.split("=") for i in cni_args.split(";"))
        namespace = cni_args_dict['K8S_POD_NAMESPACE']
        pod_name = cni_args_dict['K8S_POD_NAME']
        container_id = cni_args_dict['K8S_POD_INFRA_CONTAINER_ID']
    except Exception as e:
        raise JAGUARCNIException(100, 'required CNI variables missing', str(e))

    if cni_command == "ADD":
        cni_add(cni_ifname, cni_netns, namespace, pod_name, container_id)
    elif cni_command == "DEL":
        cni_del(container_id)

if __name__ == '__main__':
    try:
        main()
    except JAGUARCNIException as e:
        print(e.cni_error())
        sys.exit(1)
    except Exception as e:
        error = {'cniVersion': CNI_VERSION, 'code': 100,
                 'message': str(e)}
        print(json.dumps(error))
        sys.exit(1)
